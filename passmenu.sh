#!/bin/bash

if [ ! -z "${@}" ]; then
    notify-send "${@}"
    coproc ( pass -c "${@}"  > /dev/null  2>1 )
    exit 0;
fi

passwords=$(./list_passwords.sh)

echo -en "\0no-custom\x1ftrue\n"
echo -en "\0urgent\x1ftrue\n"
echo -en "\0message\x1ftest-message\n"
echo -en "aap\0icon\x1ffolder\0nonselectable\x1ftrue\n"
printf '%s\n' "${passwords}"
