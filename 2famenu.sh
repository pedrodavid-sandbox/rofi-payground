#!/bin/bash

# TODO: What tha hell is this: https://stackoverflow.com/a/13864829

if [[ "${@}" == "Show all" ]]; then
    echo -en "\0message\x1fPassword files\n"
elif [[ "${@}" == "Reload" ]]; then
    echo -en "\0message\x1fReloading ...\n"
elif [[ ! -z "${@}" ]]; then
    coproc ( 2fa -c "${@}"  > /dev/null  2>&1 );
    exit 0;
fi

function update_2fa-list_cache () {
    mkdir -p ~/.cache/2famenu

    # Strip Colors: https://stackoverflow.com/a/18000433
    pass grep '2FA: ' -lq | sed '/^$/d;s/:$//' | sed -r "s/\x1B\[([0-9]{1,3}(;[0-9]{1,2})?)?[mGK]//g" > ~/.cache/2famenu/2fa_list 2>/dev/null &
}

if [ -s ~/.cache/2famenu/2fa_list ]; then
    echo -e "Reload"
    echo -e "Show all"
    passwords=$(cat ~/.cache/2famenu/2fa_list);
else
    echo -e "Reload"
    passwords=$(./list_passwords.sh)
fi

printf '%s\n' "${passwords}"

update_2fa-list_cache
